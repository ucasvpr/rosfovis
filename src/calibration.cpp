#include "rosfovis/calibration.hpp"

namespace ca_rosfovis {

/**
 * For the raw, unresized, distorted, unrectified images.
 * TODO: maybe we can use image_geometry
 */
bool get_calibration_raw(sensor_msgs::CameraInfoConstPtr l_cam_info,
                         sensor_msgs::CameraInfoConstPtr r_cam_info,
                         fovis::StereoCalibrationParameters& stereo_params
                         ) {

  fovis::CameraIntrinsicsParameters * params;
  sensor_msgs::CameraInfoConstPtr cam_info;
  Eigen::Matrix3d RL, RR;
  Eigen::Matrix3d *Rptr;
  for (int i=0; i < 2; ++i) {
    if (i == 0) {
      params = &(stereo_params.left_parameters);
      cam_info = l_cam_info;
      Rptr = &RL;
    } else {
      params = &(stereo_params.right_parameters);
      cam_info = r_cam_info;
      Rptr = &RR;
    }
    params->width  = cam_info->width;
    params->height = cam_info->height;
    params->fx     = cam_info->K[0];
    params->fy     = cam_info->K[4];
    params->cx     = cam_info->K[2];
    params->cy     = cam_info->K[5];
    params->k1     = cam_info->D[0];
    params->k2     = cam_info->D[1];
    params->k3     = cam_info->D[4];
    params->p1     = cam_info->D[2];
    params->p2     = cam_info->D[3];

    // note we are getting R^-1 here, hence R(j, i)
    for (int i=0; i < 3; ++i) {
      for (int j=0; j < 3; ++j) {
        (*Rptr)(j, i) = cam_info->R[j*3+i];
      }
    }
  }

  // (1) L_raw     (4)R_raw
  //      |         |
  //      |         |
  // (2) L_rect----(3)R_rect
  Eigen::Matrix4d T_12 = Eigen::Matrix4d::Identity();
  T_12.block<3,3>(0, 0) = RL;
  Eigen::Matrix4d T_34 = Eigen::Matrix4d::Identity();
  T_34.block<3,3>(0, 0) = RR.inverse();
  Eigen::Matrix4d T_23 = Eigen::Matrix4d::Identity();
  T_23(0, 3) = r_cam_info->P[3]/r_cam_info->P[0];

  Eigen::Matrix4d T_14 = T_12*T_23*T_34;

  Eigen::Quaterniond q(T_14.block<3,3>(0,0));
  //q = q.inverse();
  stereo_params.right_to_left_rotation[0] = q.w();
  stereo_params.right_to_left_rotation[1] = q.x();
  stereo_params.right_to_left_rotation[2] = q.y();
  stereo_params.right_to_left_rotation[3] = q.z();

  stereo_params.right_to_left_translation[0] = T_14(0, 3);
  stereo_params.right_to_left_translation[1] = T_14(1, 3);
  stereo_params.right_to_left_translation[2] = T_14(2, 3);

  //std::cout << "rot:\n" << q.w() << "," << q.x() << "," << q.y() << "," << q.z() << std::endl;
  //std::cout << "trans:\n" << T_14.block<3,1>(0, 3) << std::endl;
  return true;
}

/**
 * This is for the rectified+undistorted and possibly downsampled images.
 * TODO: maybe we can use image_geometry
 */
bool get_calibration(sensor_msgs::CameraInfoConstPtr l_cam_info,
                     sensor_msgs::CameraInfoConstPtr r_cam_info,
                     bool resized,
                     fovis::StereoCalibrationParameters& stereo_params
                     ) {

  // Projection/camera matrix. Row major.
  //     [fx'  0  cx' Tx]
  // P = [ 0  fy' cy' Ty]
  //     [ 0   0   1   0]
  // account for image resizing. Note half-resolution is assumed.
  double resize_factor = (resized ? 0.5 : 1.0);
  stereo_params.left_parameters.width  = resize_factor*l_cam_info->width;
  stereo_params.left_parameters.height = resize_factor*l_cam_info->height;
  stereo_params.left_parameters.fx     = resize_factor*l_cam_info->P[0];
  stereo_params.left_parameters.fy     = resize_factor*l_cam_info->P[5];
  stereo_params.left_parameters.cx     = resize_factor*l_cam_info->P[2];
  stereo_params.left_parameters.cy     = resize_factor*l_cam_info->P[6];
  stereo_params.left_parameters.k1     = 0;
  stereo_params.left_parameters.k2     = 0;
  stereo_params.left_parameters.k3     = 0;
  stereo_params.left_parameters.p1     = 0;
  stereo_params.left_parameters.p2     = 0;

  stereo_params.right_parameters = stereo_params.left_parameters;

  // quaternion wxyz.
  stereo_params.right_to_left_rotation[0] = 1;
  stereo_params.right_to_left_rotation[1] = 0;
  stereo_params.right_to_left_rotation[2] = 0;
  stereo_params.right_to_left_rotation[3] = 0;

  // physical parameters. baseline = Tx/fx'.
  stereo_params.right_to_left_translation[0] = r_cam_info->P[3]/r_cam_info->P[0];
  stereo_params.right_to_left_translation[1] = 0;
  stereo_params.right_to_left_translation[2] = 0;

  //ROS_INFO("baseline: %f", stereo_params.right_to_left_translation[0]);
  //std::cout << "rot:\n" << q.w() << "," << q.x() << "," << q.y() << "," << q.z() << std::endl;
  //std::cout << "trans:\n" << T_13.block<3,1>(0, 3) << std::endl;

  return true;
}

}
