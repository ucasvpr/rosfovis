
syms fx fy cx cy real
syms x y real
syms gx gy real


K = [fx 0 cx; 0 fy cy; 0 0 1];
G1 = [0 0 0 ; 0 0 1; 0 -1 0];
G2 = [0 0 -1; 0 0 0; 1 0 0];
G3 = [0 1 0; -1 0 0; 0 0 0];

Kinv = inv(K)

H = K * G1 * Kinv

p1 = (K * G1 * Kinv) * [x; y; 1]
p1 = p1 ./ repmat(p1(3,:), 3, 1)

p2 = (Kinv * G2 * K) * [x; y; 1]
p2 = p2 ./ repmat(p2(3,:), 3, 1)


[gx, gy, 1/(gx + gy)]

syms h11 h12 h13 h21 h22 h23 h31 h32 h33 real
H = [h11 h12 h13; h21 h22 h23; h31 h32 h33]

pp = H*[ x; y; 1]
%pp = pp./repmat(pp(3), 3, 1)
%diff(pp, h33)
