#include <string>

#include <boost/scoped_ptr.hpp>

// ros & friends
#include <opencv2/core/core.hpp>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/PoseStamped.h>
#include <image_geometry/pinhole_camera_model.h>
#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

// ca stacks
#include <tictoc_profiler/profiler.hpp>
#include <geom_cast/geom_cast.hpp>

// this pkg
#include "fovis/fovis.hpp"
#include "fovis/visual_odometry.hpp"

#include "rosfovis/fovis_state.h"
#include "rosfovis/fovis_update.h"
#include "rosfovis/fovis_info.h"
#include "rosfovis/ros_visualization.hpp"

namespace ca
{

static const int SYNC_QUEUE_SIZE = 10;
static const int IMAGE_SUB_QUEUE_SIZE = 10;
static const int DEPTH_SUB_QUEUE_SIZE = 10;

/**
 * these are parameters that are about the node,
 * not about fovis itself
 */
struct RgbdOdometryNodeParameters {
  bool use_approx_sync;
  bool enable_profiling;

  bool ros_visualize;

  bool publish_pose;
  bool publish_odom_accum;
  bool publish_odom_relative;
  bool publish_fovis_update;

  std::string odom_frame_id;
  std::string base_frame_id;

  RgbdOdometryNodeParameters() :
      use_approx_sync(false),
      enable_profiling(true),
      ros_visualize(true),
      publish_pose(true),
      publish_odom_accum(true),
      publish_odom_relative(true),
      publish_fovis_update(true),
      odom_frame_id("/odom"),
      base_frame_id("/base_frame")
  { }

  void get_from_node_handle(ros::NodeHandle& nh) {

    if (!nh.getParam("use_approx_sync", use_approx_sync)) { ; }
    if (!nh.getParam("enable_profiling", enable_profiling)) { ; }
    if (!nh.getParam("ros_visualize", ros_visualize)) { ; }
    if (!nh.getParam("publish_pose", publish_pose)) { ; }
    if (!nh.getParam("publish_odom_accum", publish_odom_accum)) { ; }
    if (!nh.getParam("publish_odom_relative", publish_odom_relative)) { ; }
    if (!nh.getParam("publish_fovis_update", publish_fovis_update)) { ; }
    if (!nh.getParam("odom_frame_id", odom_frame_id)) { ; }
    if (!nh.getParam("base_frame_id", base_frame_id)) { ; }

  }
};

class RgbdOdometryNode {
public:
  typedef message_filters::sync_policies::ExactTime<sensor_msgs::Image,
                                                    sensor_msgs::Image,
                                                    sensor_msgs::CameraInfo,
                                                    sensor_msgs::CameraInfo> ExactPolicy;
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image,
                                                          sensor_msgs::Image,
                                                          sensor_msgs::CameraInfo,
                                                          sensor_msgs::CameraInfo> ApproximatePolicy;
  typedef message_filters::Synchronizer<ExactPolicy> ExactSync;
  typedef message_filters::Synchronizer<ApproximatePolicy> ApproximateSync;

public:
  RgbdOdometryNode() :
      nh_("~"),
      it_(nh_),
      exact_sync_(NULL),
      approx_sync_(NULL),
      odom_(NULL),
      depth_producer_(NULL),
      rectification_(NULL),
      depth_img_width_(-1),
      depth_img_height_(-1),
      image_count_(0),
      got_calibration_(false),
      current_ts_(0),
      last_ts_(0),
      odom_seq("0")
  {
    init();
  }

  virtual ~RgbdOdometryNode() {}

 private:

  // TODO pretty much same as in stereo_odometry.
  // TODO factor out this code
  void publish_measurements() {

    const fovis::MotionEstimator * me = odom_->getMotionEstimator();

    Eigen::Vector3d translation(Eigen::Vector3d::Zero());
    Eigen::Quaterniond rotation(Eigen::Quaterniond::Identity());
    {
      Eigen::Isometry3d cam_to_local = odom_->getPose();
      // rotate coordinate frame so that look vector is +X, and down is +Z
      Eigen::Matrix3d M;
      M <<  0,  0, 1,
            1,  0, 0,
            0,  1, 0;
      cam_to_local = M * cam_to_local;
      translation = cam_to_local.translation();
      rotation = cam_to_local.rotation();
      rotation = rotation * M.transpose();
    }

    geometry_msgs::PoseStamped pose_msg;
    {
      // accumulated pose message.
      pose_msg.header.stamp = current_ts_;
      pose_msg.header.frame_id = params_.odom_frame_id;
      pose_msg.pose.position = ca::point_cast<geometry_msgs::Point>(translation);
      pose_msg.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(rotation);
    }

    // accumulated odometry message, same info as above.
    nav_msgs::Odometry accum_odom_msg;
    {
      accum_odom_msg.header.stamp = current_ts_;
      accum_odom_msg.header.frame_id = params_.odom_frame_id;
      accum_odom_msg.child_frame_id = params_.base_frame_id;
      accum_odom_msg.pose.pose.position = ca::point_cast<geometry_msgs::Point>(translation);
      accum_odom_msg.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(rotation);
    }

    // relative pose message.
    Eigen::Isometry3d motion(odom_->getMotionEstimate());
    Eigen::Isometry3d motion_estimate=motion.inverse();
    Eigen::Vector3d motion_T(motion_estimate.translation());
    Eigen::Quaterniond motion_R(Eigen::Quaterniond(motion_estimate.rotation()));
    const Eigen::MatrixXd & motion_cov(odom_->getMotionEstimateCov());

    nav_msgs::Odometry odom_msg;
    {
      odom_msg.header.stamp = current_ts_;
      odom_msg.child_frame_id = "cam_" + std::string(odom_seq.str());
      odom_seq.str("");
      odom_seq << image_count_;
      odom_msg.header.frame_id = "cam_" + std::string(odom_seq.str());

      // TODO change this for conformance to libviso-type output
      // odom_msg.header.frame_id = "/prev_frame";
      // odom_msg.child_frame_id = "/cur_frame";

      odom_msg.pose.pose.position = ca::point_cast<geometry_msgs::Point>(motion_T);
      odom_msg.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(motion_R);
    }

    // TODO check if the conventions are correct
    double dt = (current_ts_ - last_ts_).toSec();
    if (dt > 0) {
      Eigen::Vector3d linear(motion_T/dt);
      odom_msg.twist.twist.linear = ca::point_cast<geometry_msgs::Vector3>(linear);
      tf::Quaternion tfq = ca::rot_cast<tf::Quaternion>(motion_R);

      double angle = tfq.getAngle();
      tf::Vector3 axis = tfq.getAxis();
      tf::Vector3 twist = axis * (angle/dt);
      odom_msg.twist.twist.angular = ca::point_cast<geometry_msgs::Vector3>(twist);
      for (int i=0;i<6;++i) {
        for (int j=0;j<6;++j) {
          odom_msg.pose.covariance[j*6+i] = motion_cov(i, j);
        }
      }
    }
    // data about how fovis is doing
    rosfovis::fovis_update update_msg;
    update_msg.header.stamp = current_ts_;

    fovis::MotionEstimateStatusCode estim_status = odom_->getMotionEstimateStatus();
    switch (estim_status) {
      case fovis::NO_DATA:
        update_msg.info.estimate_status = rosfovis::fovis_update::NO_DATA;
        break;
      case fovis::SUCCESS:
        update_msg.info.estimate_status = rosfovis::fovis_update::ESTIMATE_VALID;

        // TODO why is this commented out?
        //ROS_INFO("Inliers: %4d  Rep. fail: %4d Matches: %4d Feats: %4d Mean err: %5.2f",
        //         me->getNumInliers(),
        //         me->getNumReprojectionFailures(),
        //         me->getNumMatches(),
        //         odom_->getTargetFrame()->getNumKeypoints(),
        //         me->getMeanInlierReprojectionError());

        break;
      case fovis::INSUFFICIENT_INLIERS:
        update_msg.info.estimate_status = rosfovis::fovis_update::ESTIMATE_INSUFFICIENT_FEATURES;
        ROS_INFO("FOVIS::Insufficient inliers: %4d ", me->getNumInliers());
        break;
      case fovis::OPTIMIZATION_FAILURE:
        update_msg.info.estimate_status = rosfovis::fovis_update::ESTIMATE_DEGENERATE;
        ROS_INFO("FOVIS::Unable to solve for rigid body transform");
        break;
      case fovis::REPROJECTION_ERROR:
        update_msg.info.estimate_status = rosfovis::fovis_update::ESTIMATE_REPROJECTION_ERROR;
        ROS_INFO("FOVIS::Excessive reprojection error (%f). and number of inliers: %4d and number of matches %4d",
                 me->getMeanInlierReprojectionError(),
                 me->getNumInliers(), me->getNumMatches());
        break;
      default:
        ROS_ERROR("FOVIS::Unknown error (this should never happen)");
        break;
    }

    update_msg.info.change_reference_frame = odom_->getChangeReferenceFrames();
    update_msg.info.num_matches = me->getNumMatches();
    update_msg.info.num_inliers = me->getNumInliers();
    update_msg.info.mean_reprojection_error = me->getMeanInlierReprojectionError();
    update_msg.info.num_reprojection_failures = me->getNumReprojectionFailures();
    const fovis::OdometryFrame * tf(odom_->getTargetFrame());
    update_msg.info.num_detected_keypoints = tf->getNumDetectedKeypoints();
    update_msg.info.num_keypoints = tf->getNumKeypoints();
    update_msg.info.fast_threshold = odom_->getFastThreshold();

    //Complete fovis_state msg, with estimation status, pose with covariance etc.
    rosfovis::fovis_state state_msg;
    state_msg.header.stamp = current_ts_;
    state_msg.header.frame_id = params_.odom_frame_id;
    state_msg.referenceStamp = last_ts_;
    state_msg.info = update_msg.info;
    state_msg.pose = odom_msg.pose;
    state_msg.twist = odom_msg.twist;
    fovis_state_pub_.publish(state_msg);

    // TODO should we only publish if success?
    if (estim_status != fovis::NO_DATA) {
      if (params_.publish_pose) { pose_pub_.publish(pose_msg); }
      if (params_.publish_odom_accum) {
        accum_odom_pub_.publish(accum_odom_msg);
      }
      if (params_.publish_odom_relative) {
        rel_odom_pub_.publish(odom_msg);
      }
    }

    if (params_.publish_fovis_update) { fovis_update_pub_.publish(update_msg); }

    if (params_.ros_visualize && image_vis_pub_.getNumSubscribers() > 0) {
      ca::Profiler::tictoc("ros_viz");
      ros_visualization_->draw(odom_.get(), out_buffer_);
      ca::Profiler::tictoc("ros_viz");
      cv_bridge::CvImage img_msg;
      img_msg.header.stamp = current_ts_;
      img_msg.header.frame_id = params_.base_frame_id;
      img_msg.encoding = sensor_msgs::image_encodings::BGR8;
      img_msg.image = out_buffer_;
      image_vis_pub_.publish(img_msg.toImageMsg());
    }
  }

  fovis::VisualOdometryOptions get_vo_parameters() {
    fovis::VisualOdometryOptions vo_opts = fovis::VisualOdometry::getDefaultOptions();
    fovis::VisualOdometryOptions def_vo_opts = fovis::VisualOdometry::getDefaultOptions();

    nh_.param<std::string>("feature_window_size"                 , vo_opts["feature-window-size"]                 , def_vo_opts["feature-window-size"]                );
    nh_.param<std::string>("max_pyramid_level"                   , vo_opts["max-pyramid-level"]                   , def_vo_opts["max-pyramid-level"]                  );
    nh_.param<std::string>("min_pyramid_level"                   , vo_opts["min-pyramid-level"]                   , def_vo_opts["min-pyramid-level"]                  );
    nh_.param<std::string>("target_pixels_per_feature"           , vo_opts["target-pixels-per-feature"]           , def_vo_opts["target-pixels-per-feature"]          );
    nh_.param<std::string>("fast_threshold"                      , vo_opts["fast-threshold"]                      , def_vo_opts["fast-threshold"]                     );
    nh_.param<std::string>("fast_threshold_adaptive_gain"        , vo_opts["fast-threshold-adaptive-gain"]        , def_vo_opts["fast-threshold-adaptive-gain"]       );
    nh_.param<std::string>("use_adaptive_threshold"              , vo_opts["use-adaptive-threshold"]              , def_vo_opts["use-adaptive-threshold"]             );
    nh_.param<std::string>("use_homography_initialization"       , vo_opts["use-homography-initialization"]       , def_vo_opts["use-homography-initialization"]      );
    nh_.param<std::string>("ref_frame_change_threshold"          , vo_opts["ref-frame-change-threshold"]          , def_vo_opts["ref-frame-change-threshold"]         );
    nh_.param<std::string>("initial_rotation_pyramid_level"      , vo_opts["initial-rotation-pyramid-level"]      , def_vo_opts["initial-rotation-pyramid-level"]     );

    // OdometryFrame
    nh_.param<std::string>("use_bucketing"                       , vo_opts["use-bucketing"]                       , def_vo_opts["use-bucketing"]                      );
    nh_.param<std::string>("bucket_width"                        , vo_opts["bucket-width"]                        , def_vo_opts["bucket-width"]                       );
    nh_.param<std::string>("bucket_height"                       , vo_opts["bucket-height"]                       , def_vo_opts["bucket-height"]                      );
    nh_.param<std::string>("max_keypoints_per_bucket"            , vo_opts["max-keypoints-per-bucket"]            , def_vo_opts["max-keypoints-per-bucket"]           );
    nh_.param<std::string>("use_image_normalization"             , vo_opts["use-image-normalization"]             , def_vo_opts["use-image-normalization"]            );

    // MotionEstimator
    nh_.param<std::string>("inlier_max_reprojection_error"       , vo_opts["inlier-max-reprojection-error"]       , def_vo_opts["inlier-max-reprojection-error"]      );
    nh_.param<std::string>("clique_inlier_threshold"             , vo_opts["clique-inlier-threshold"]             , def_vo_opts["clique-inlier-threshold"]            );
    nh_.param<std::string>("min_features_for_estimate"           , vo_opts["min-features-for-estimate"]           , def_vo_opts["min-features-for-estimate"]          );
    nh_.param<std::string>("max_mean_reprojection_error"         , vo_opts["max-mean-reprojection-error"]         , def_vo_opts["max-mean-reprojection-error"]        );
    nh_.param<std::string>("use_subpixel_refinement"             , vo_opts["use-subpixel-refinement"]             , def_vo_opts["use-subpixel-refinement"]            );
    nh_.param<std::string>("feature_search_window"               , vo_opts["feature-search-window"]               , def_vo_opts["feature-search-window"]              );
    nh_.param<std::string>("update_target_features_with_refined" , vo_opts["update-target-features-with-refined"] , def_vo_opts["update-target-features-with-refined"]);
ROS_INFO_STREAM("get default options max mean REPROJECTION ERROR" <<vo_opts["max-mean-reprojection-error"]   );
    return vo_opts;
  }

  void subscribe() {
    // TODO use conventional names for these
    // TODO option for alternative transports (jpg?)
    image_sub_.subscribe(it_, "rectified/rgb/image", IMAGE_SUB_QUEUE_SIZE);
    depth_sub_.subscribe(it_, "rectified/depth/image", DEPTH_SUB_QUEUE_SIZE);
    image_info_sub_.subscribe(nh_, "rectified/rgb/camera_info", IMAGE_SUB_QUEUE_SIZE);
    depth_info_sub_.subscribe(nh_, "rectified/depth/camera_info", DEPTH_SUB_QUEUE_SIZE);
    if (params_.use_approx_sync) {
      approx_sync_.reset(new ApproximateSync(ApproximatePolicy(SYNC_QUEUE_SIZE),
                                             image_sub_, depth_sub_,
                                             image_info_sub_, depth_info_sub_));
      approx_sync_->registerCallback(boost::bind(&RgbdOdometryNode::image_cb,
                                                this,
                                                _1, _2, _3, _4));
    } else {
      exact_sync_.reset(new ExactSync(ExactPolicy(SYNC_QUEUE_SIZE),
                                image_sub_,
                                depth_sub_,
                                image_info_sub_,
                                depth_info_sub_));
      exact_sync_->registerCallback(boost::bind(&RgbdOdometryNode::image_cb,
                                                this,
                                                _1, _2, _3, _4));

    }

  }

  void advertise() {
    // TODO convention-conformant default names
    pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("pose", 1);
    accum_odom_pub_ = nh_.advertise<nav_msgs::Odometry>("fovis/accumulated/pose", 1);
    rel_odom_pub_ = nh_.advertise<nav_msgs::Odometry>("fovis/measurement", 1);
    fovis_update_pub_ = nh_.advertise<rosfovis::fovis_update>("fovis/fovis_update", 1);
    fovis_state_pub_ = nh_.advertise<rosfovis::fovis_state>("fovis/fovis_state",1);
    if (params_.ros_visualize) {
      ROS_INFO("ROS visualization enabled.");
      image_vis_pub_ = it_.advertise("fovis/vo_vis", 1);
    }
  }

  void init() {
    params_.get_from_node_handle(nh_);

    if (params_.enable_profiling) {
      ROS_INFO("Profiling enabled");
      ca::Profiler::enable();
    }

    this->subscribe();
    this->advertise();
  }

  /**
   * This is just about the RGB image intrinsics.
   * This assumes rectified+undistorted and possibly downsampled images.
   * TODO: verify image_geometry does what we want
   */
  void get_calibration(sensor_msgs::CameraInfoConstPtr camera_info,
                       sensor_msgs::CameraInfoConstPtr depth_info) {
    ROS_INFO("getting calibration from camerainfo");
    image_geometry::PinholeCameraModel camera_model;
    camera_model.fromCameraInfo(camera_info);
    rgb_intrinsics_.cx = camera_model.cx();
    rgb_intrinsics_.cy = camera_model.cy();
    rgb_intrinsics_.fx = camera_model.fx();
    rgb_intrinsics_.fy = camera_model.fy();
    rgb_intrinsics_.width = camera_model.reducedResolution().width;
    rgb_intrinsics_.height = camera_model.reducedResolution().height;
    rectification_.reset(new fovis::Rectification(rgb_intrinsics_));

    depth_img_width_ = depth_info->width;
    depth_img_height_ = depth_info->height;

    got_calibration_ = true;
  }

  void init_vo() {
    ROS_INFO("initializing VO object");
    // this has to be delayed until we get calibration from camerainfo
    ROS_ASSERT(got_calibration_);

    fovis::VisualOdometryOptions vo_opts(this->get_vo_parameters());
    ROS_INFO_STREAM("max_mean_reprojection_error = " << vo_opts["max-mean-reprojection-error"]);
    odom_.reset(new fovis::VisualOdometry(rectification_.get(), vo_opts));

    // TODO use rgb params or depth params?
    depth_producer_.reset(new fovis::DepthImage(rgb_intrinsics_, depth_img_width_, depth_img_height_));

    if (params_.ros_visualize) { ros_visualization_.reset(new FovisVisualization()); }
  }

  void image_cb(const sensor_msgs::ImageConstPtr& image,
                const sensor_msgs::ImageConstPtr& depth_image,
                const sensor_msgs::CameraInfoConstPtr& cam_info,
                const sensor_msgs::CameraInfoConstPtr& depth_cam_info) {
    ++image_count_;
    last_ts_ = current_ts_;
    current_ts_ = image->header.stamp;

    if (!got_calibration_) {
      this->get_calibration(cam_info, depth_cam_info);
      this->init_vo();
    }

    // get depth data
    ROS_ASSERT(depth_image->encoding==sensor_msgs::image_encodings::TYPE_32FC1);
    ROS_ASSERT(depth_image->step==depth_image->width*sizeof(float));

    uint8_t *gray_data(NULL);
    ca::Profiler::tictoc("convert_image");
    // TODO what encodings do we expect?
    // TODO add jpg support
    // for now we assume gray. see stereo_odometry for rgb debayering/decoding
    // Convert ROS messages for use with OpenCV
    try {
      cv_bridge::CvImageConstPtr cvimage = cv_bridge::toCvShare(image,
                                                                sensor_msgs::image_encodings::MONO8);
      gray_data = cvimage->image.data;
    } catch (cv_bridge::Exception& e) {
      ROS_ERROR("Conversion error: %s", e.what());
      return;
    }
    ca::Profiler::tictoc("convert_image");

    //cv::imshow("rgb", gray_buffer_);
    //cv::waitKey(10);

    ca::Profiler::tictoc("vo");
    depth_producer_->setDepthImage(reinterpret_cast<const float*>(depth_image->data.data()));
    odom_->processFrame(gray_data, depth_producer_.get());
    ca::Profiler::tictoc("vo");

    this->publish_measurements();
  }

private:
  RgbdOdometryNode(const RgbdOdometryNode& other);
  RgbdOdometryNode& operator=(const RgbdOdometryNode& other);

private:
  // data
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;

  // Subscriptions
  image_transport::SubscriberFilter image_sub_;
  image_transport::SubscriberFilter depth_sub_;
  message_filters::Subscriber<sensor_msgs::CameraInfo> image_info_sub_;
  message_filters::Subscriber<sensor_msgs::CameraInfo> depth_info_sub_;
  boost::scoped_ptr<ExactSync> exact_sync_;
  boost::scoped_ptr<ApproximateSync> approx_sync_;

  // Publishers
  ros::Publisher pose_pub_;
  ros::Publisher accum_odom_pub_;
  ros::Publisher rel_odom_pub_;
  ros::Publisher fovis_update_pub_;
  ros::Publisher fovis_state_pub_;
  image_transport::Publisher image_vis_pub_;

  boost::scoped_ptr<fovis::VisualOdometry> odom_;
  boost::scoped_ptr<fovis::DepthImage> depth_producer_;
  fovis::CameraIntrinsicsParameters rgb_intrinsics_;
  boost::scoped_ptr<fovis::Rectification> rectification_;
  int depth_img_width_, depth_img_height_;

  boost::scoped_ptr<FovisVisualization> ros_visualization_;

  cv::Mat out_buffer_;
  cv::Mat gray_buffer_;

  uint64_t image_count_;
  bool got_calibration_;

  ros::Time current_ts_, last_ts_;
  std::stringstream odom_seq;
  RgbdOdometryNodeParameters params_;
};

} /* ca */

int main(int argc, char** argv) {

  ros::init(argc, argv, "rgbd_odometry");

  ca::RgbdOdometryNode rgbdo_node;

  while(ros::ok()) {
    ros::spinOnce();
  }

  ROS_INFO("done");
  ca::Profiler::print_aggregated(std::cout);
  return 0;
}
