#ifndef ALGO_UTIL_HPP_ZB5UF8WS
#define ALGO_UTIL_HPP_ZB5UF8WS

#include <cmath>

#include <algorithm>
#include <iterator>
#include <functional>

#include <Eigen/Core>

namespace ca
{

#define CA_STATIC_ARRAY_SIZE(x) (sizeof((x))/sizeof((x[0])))

/**
 * Unlike c modulus (%) does not return negative numbers.
 * It's more like python modulus.
 */
template<typename T>
T mod_wrap(T x, T y) {
  return (x - std::floor(static_cast<double>(x)/y)*y);
}

template<typename T>
void inplace_mod_wrap(T& x, T y) {
  x -= (std::floor(static_cast<double>(x)/y)*y);
}

template <class Iterator>
Iterator argmin(Iterator first, Iterator last) {
  if (first==last) { return first; }
  Iterator ret = first;
  for (++first; first != last; ++first) {
    if (*first < *ret) {
       ret = first;
    }
  }
  return ret;
}

template <class Iterator>
Iterator argmax(Iterator first, Iterator last) {
  if (first==last) { return first; }
  Iterator ret = first;
  for (++first; first != last; ++first) {
    if (*ret < *first) {
       ret = first;
    }
  }
  return ret;
}

template <class Iterator, class Comparator>
Iterator argmin(Iterator first, Iterator last, Comparator comp) {
  if (first==last) { return first; }
  Iterator ret = first;
  for (++first; first != last; ++first) {
    if (comp(*first, *ret)) {
       ret = first;
    }
  }
  return ret;
}

template <class Iterator, class Comparator>
Iterator argmax(Iterator first, Iterator last, Comparator comp) {
  if (first==last) { return first; }
  Iterator ret = first;
  for (++first; first != last; ++first) {
    if (comp(*ret, *first)) {
       ret = first;
    }
  }
  return ret;
}

template <class PairType>
struct PairSecondLessComparator : public std::unary_function<PairType, bool> {
  bool operator()(const PairType& pair1, const PairType& pair2) {
    return (pair1.second < pair2.second);
  }
};

#if 0
template <class K, class V>
struct MapValueLessComparator {
 public:
  bool operator()(const std::pair<K, V>& pair1, const std::pair<K, V>& pair2) {
    return (pair1.second < pair2.second);
  }
};

template <class PairIterator>
typename std::iterator_traits<typename PairIterator>::value_type ::first_type pair_argmin(PairIterator first, PairIterator last) {
  if (first==last) { return first; }
  typename PairIterator::first_type ret = first->first;
  for (++first; first != last; ++first) {
    if (first->second < ret->second) {
       ret = first;
    }
  }
  return ret;
}
#endif

template <class MapType>
typename MapType::key_type map_argmin(const MapType& m) {
  typename MapType::const_iterator begin_itr = m.begin();
  typename MapType::const_iterator end_itr = m.end();
  if (begin_itr==end_itr) { return begin_itr->first; }
  typename MapType::const_iterator ret = begin_itr;
  for (++begin_itr; begin_itr != end_itr; ++begin_itr) {
    if (begin_itr->second < ret->second) {
       ret = begin_itr;
    }
  }
  return ret->first;
}

template <class MapType>
typename MapType::key_type map_argmax(const MapType& m) {
  typename MapType::const_iterator begin_itr = m.begin();
  typename MapType::const_iterator end_itr = m.end();
  if (begin_itr==end_itr) { return begin_itr->first; }
  typename MapType::const_iterator ret = begin_itr;
  for (++begin_itr; begin_itr != end_itr; ++begin_itr) {
    if ( ret->second < begin_itr->second ) {
       ret = begin_itr;
    }
  }
  return ret->first;
}

template <class Iterator, class T>
void lin_seq(Iterator first, Iterator last, T start, T step) {
  for (; first != last; ++first, start += step) {
    *first = start;
  }
}

template <class Iterator, class T>
void lin_seq(Iterator first, Iterator last, T start) {
  for (; first != last; ++first, ++start) {
    *first = start;
  }
}


template <class Iterator>
void lin_seq(Iterator first, Iterator last) {
  typedef typename std::iterator_traits<Iterator>::value_type Value;
  Value val(0);
  lin_seq(first, last, val);
}

template <class Iterator, class Compare>
struct IndexCompare {
 public:
  IndexCompare(Iterator i, Compare c) :
      i_(i),
      c_(c)
  { }

  Iterator i_;
  Compare c_;

  template <class Index>
  bool operator()(Index const & l, Index const & r) const {
    return c_(i_[l], i_[r]);
  }
};

template <class Iterator, class IndexIterator, class Compare>
void index_sort(Iterator first, Iterator last, IndexIterator index_first, Compare c) {
  int size = last - first; // TODO use std::distance?
  lin_seq(index_first, index_first+size);
  std::sort(index_first,
            index_first+size,
            IndexCompare<Iterator, Compare>(first, c));
}

template <class Iterator, class IndexIterator>
void index_sort(Iterator first, Iterator last, IndexIterator index_first) {
  typedef typename std::iterator_traits<Iterator>::value_type Value;
  index_sort(first, last, index_first, std::less<Value>());
}

template <class IndexIterator, class InIterator, class OutIterator>
void apply_permutation(IndexIterator index_first,
                       IndexIterator index_last,
                       InIterator in,
                       OutIterator out) {
  for(; index_first != index_last; ++index_first, ++out) {
    *out = in[*index_first];
  }
}

template <class T>
void update_min(T & x, const T & y) {
  x = std::min(x, y);
}

template <class T>
void update_max(T & x, const T & y) {
  x = std::max(x, y);
}

template <class T>
void update_clip(T & x, const T & lim_inf, const T & lim_sup) {
  x = std::min(x, lim_sup);
  x = std::max(x, lim_inf);
}

} /* ca */

#endif /* end of include guard: ALGO_UTIL_HPP_ZB5UF8WS */
