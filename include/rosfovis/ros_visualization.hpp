#ifndef _ROS_VISUALIZATION_H
#define _ROS_VISUALIZATION_H

#include <ros/ros.h>
#include <ros/console.h>

#include <opencv2/core/core.hpp>

namespace fovis
{
class VisualOdometry;
class StereoCalibration;
struct PyramidLevel;
}

namespace ca
{

struct FovisVisualizationParameters {

  bool viz_keypoint_cloud; // TODO
  bool viz_keypoint_2d;
  bool viz_keypoint_patch;
  bool viz_flow;
  bool viz_homo;
  bool viz_image_pyramid; // TODO
  // TODO these are hacks and should be fixed
  double min_z;
  double max_z;

  FovisVisualizationParameters() :
      viz_keypoint_cloud(false),
      viz_keypoint_2d(true),
      viz_keypoint_patch(false),
      viz_flow(true),
      viz_homo(true),
      viz_image_pyramid(false),
      min_z(1.),
      max_z(6.)
  { }

  void get_from_node_handle(ros::NodeHandle& nh) {
    if (!nh.getParam("viz_keypoint_cloud", viz_keypoint_cloud)) { ; }
    if (!nh.getParam("viz_keypoint_2d", viz_keypoint_2d)) { ; }
    if (!nh.getParam("viz_keypoint_patch", viz_keypoint_patch)) { ; }
    if (!nh.getParam("viz_image_pyramid", viz_image_pyramid)) { ; }
    if (!nh.getParam("viz_flow", viz_flow)) { ; }
    if (!nh.getParam("viz_homo", viz_homo)) { ; }
    if (!nh.getParam("min_z", min_z)) { ; }
    if (!nh.getParam("max_z", max_z)) { ; }
  }

};

class FovisVisualization {
public:
  FovisVisualization();
  FovisVisualization(const fovis::StereoCalibration *calib);
  virtual ~FovisVisualization() { }

  void draw(const fovis::VisualOdometry* odom, cv::Mat& out_buffer);

private:
  FovisVisualization(const FovisVisualization& other);
  FovisVisualization& operator=(const FovisVisualization& other);

private:
  void colormap(float z, cv::Scalar& rgb);
  void draw_keypoints(const fovis::VisualOdometry* odom, cv::Mat& out_buffer);
  void draw_flow(const fovis::VisualOdometry* odom, cv::Mat& out_buffer);
  void draw_homography(const fovis::VisualOdometry* odom, cv::Mat& out_buffer);

private:

  float min_z_, max_z_;
  FovisVisualizationParameters params_;

};

}
#endif /* end of include guard: ROS_VISUALIZATION_H */
