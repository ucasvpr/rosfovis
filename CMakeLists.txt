cmake_minimum_required(VERSION 2.8.3)
project(rosfovis)

#add_definitions(-Wall -march=native -msse2 -msse3 -msse4.2)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  image_geometry
  image_transport
  message_generation
  nav_msgs
  roscpp
  sensor_msgs
  std_msgs
  tf
  tictoc_profiler
  geom_cast
  cmake_modules
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS system)
find_package(OpenCV REQUIRED)
find_package(Eigen REQUIRED)

################################################
## Declare ROS messages, services and actions ##
################################################

## Generate messages in the 'msg' folder
add_message_files(
     DIRECTORY msg
     FILES fovis_update.msg
           fovis_state.msg
           fovis_info.msg
)

## Generate added messages and services with any dependencies listed here
generate_messages(
   DEPENDENCIES
   nav_msgs 
   sensor_msgs
   std_msgs
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS} 
  ${Eigen_INCLUDE_DIRS} 
  ${Boost_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES rosfovis
  CATKIN_DEPENDS cv_bridge 
                 image_geometry 
                 image_transport 
                 nav_msgs 
                 roscpp 
                 sensor_msgs 
                 std_msgs 
                 tf 
                 message_runtime 
                 tictoc_profiler 
                 geom_cast
  DEPENDS OpenCV Eigen Boost
)

###########
## Build ##
###########
add_subdirectory(src/libfovis)

add_library(${PROJECT_NAME}_ros_visualization src/ros_visualization.cpp)
target_link_libraries(${PROJECT_NAME}_ros_visualization)

add_library(${PROJECT_NAME}_calibration src/calibration.cpp)
target_link_libraries(${PROJECT_NAME}_calibration)

option(USE_LCM "Enables bot-lcm viz" OFF)
if (USE_LCM)
    add_definitions(-DUSE_LCM)
    add_library(botlcm_visualization src/botlcm_visualization.cpp)
    target_link_libraries(botlcm_visualization lcm fovis bot2-core bot2-lcmgl-client)

    add_executable(stereo_odometry src/stereo_odometry.cpp)
    target_link_libraries(stereo_odometry ${Boost_LIBRARIES} ${OpenCV_LIBS} ${catkin_LIBRARIES})
    target_link_libraries(stereo_odometry 
        fovis ${PROJECT_NAME}_ros_visualization lcm botlcm_visualization bot2-core
        bot2-frames bot2-lcmgl-client)

    add_executable(initial_homography_estimation_tester src/initial_homography_estimation_tester.cpp)
    target_link_libraries(initial_homography_estimation_tester 
        fovis ${PROJECT_NAME}_ros_visualization lcm bot2-core
        bot2-frames bot2-lcmgl-client)

else()
    add_executable(stereo_odometry src/stereo_odometry.cpp)
    target_link_libraries(stereo_odometry 
        fovis 
        ${PROJECT_NAME}_ros_visualization 
        ${PROJECT_NAME}_calibration
        ${Boost_LIBRARIES} 
        ${OpenCV_LIBS} 
        ${catkin_LIBRARIES})

    add_executable(${PROJECT_NAME}_rgbd_odometry src/rgbd_odometry.cpp)
    target_link_libraries(${PROJECT_NAME}_rgbd_odometry ${Boost_LIBRARIES} ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})
    target_link_libraries(${PROJECT_NAME}_rgbd_odometry fovis ${PROJECT_NAME}_ros_visualization)
endif()

## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
add_dependencies(${PROJECT_NAME}_rgbd_odometry rosfovis_generate_messages_cpp)
add_dependencies(stereo_odometry rosfovis_generate_messages_cpp)
